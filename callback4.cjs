const fs = require("fs");
const getBoardFromID = require("./callback1.cjs");
const getAllListsForBoardID = require("./callback2.cjs");
const getAllCardsForListID = require("./callback3.cjs");

function getThanosInfo(boardName, listName) {
  if (typeof boardName === "string" && typeof listName === "string") {
    setTimeout(() => {
      fs.readFile("../boards.json", "utf-8", (err, data) => {
        if (err) {
          console.error(err);
        } else {
          data = JSON.parse(data);
          let board = data.find((board) => board.name === boardName);
          let boardID = board.id;
          getBoardFromID(boardID, (err, data) => {
            if (err) {
              console.error(err);
            } else {
              console.log(boardName + " board info =>", data);
              getAllListsForBoardID(boardID, (err, data) => {
                if (err) {
                  console.error(err);
                } else {
                  console.log("Lists for the Thanos board => ", data);
                  let mindId = data.find((list) => list.name === listName);
                  getAllCardsForListID(mindId.id, (err, data) => {
                    if (err) {
                      console.error(err);
                    } else {
                      console.log("Cards for Mind list =>", data);
                    }
                  });
                }
              });
            }
          });
        }
      });
    }, Math.random() * 3000);
  }else{
    console.log("All arguments should be a string");
  }
}

module.exports = getThanosInfo;
