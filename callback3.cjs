const fs = require("fs");

function getAllCardsForListID(listID, callback) {
  if (typeof listID === "string" && typeof callback === "function") {
    setTimeout(() => {
      fs.readFile("../cards.json", "utf-8", (err, data) => {
        if (err) {
          callback(err);
        } else {
          data = JSON.parse(data);
          if (data.hasOwnProperty(listID)) {
            const cards = data[listID];
            callback(null, cards);
          } else {
            callback(new Error("Cards not found for this list ID!!"));
          }
        }
      });
    }, Math.random()*2000);
  }
}

module.exports = getAllCardsForListID;


