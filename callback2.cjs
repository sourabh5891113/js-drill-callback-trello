const fs = require("fs");

function getAllListsForBoardID(boardID, callback) {
  if (typeof boardID === "string" && typeof callback === "function") {
    setTimeout(() => {
      fs.readFile("../lists_1.json", "utf-8", (err, data) => {
        if (err) {
          callback(err);
        } else {
          data = JSON.parse(data);
          if (data.hasOwnProperty(boardID)) {
            const lists = data[boardID];
            callback(null, lists);
          } else {
            callback(new Error("Lists not found for this board ID!!"));
          }
        }
      });
    }, 2000);
  }
}

module.exports = getAllListsForBoardID;
