const fs = require("fs");
const getBoardFromID = require("./callback1.cjs");
const getAllListsForBoardID = require("./callback2.cjs");
const getAllCardsForListID = require("./callback3.cjs");

function getThanosInfoAndMindSpace(boardName, listName1,listName2) {
  if (typeof boardName === "string" && typeof listName1 === "string" && typeof listName2 === "string") {
    setTimeout(() => {
      fs.readFile("../boards.json", "utf-8", (err, data) => {
        if (err) {
          console.error(err);
        } else {
          data = JSON.parse(data);
          let board = data.find((board) => board.name === boardName);
          let boardID = board.id;
          getBoardFromID(boardID, (err, data) => {
            if (err) {
              console.error(err);
            } else {
              console.log(boardName + " board info =>", data);
              getAllListsForBoardID(boardID, (err, data) => {
                if (err) {
                  console.error(err);
                } else {
                  console.log("Lists for the Thanos board => ", data);                  
                  let mind = data.find((list) => list.name === listName1);
                  let space = data.find((list) => list.name === listName2);
                  getAllCardsForListID(mind.id, (err, data) => {
                    if (err) {
                      console.error(err);
                    } else {
                      console.log("Cards for Mind list =>", data);
                    }
                  });
                  getAllCardsForListID(space.id, (err, data) => {
                    if (err) {
                      console.error(err);
                    } else {
                      console.log("Cards for space list =>", data);
                    }
                  });
                }
              });
            }
          });
        }
      });
    }, Math.random() * 3000);
  }else{
    console.log("All arguments should be a string");
  }
}

module.exports = getThanosInfoAndMindSpace;
