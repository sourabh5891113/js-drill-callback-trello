/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data 
    in lists.json. Then pass control back to the code that called it by using a callback function.
*/
const getAllListsForBoardID = require("../callback2.cjs");

let boardID = "mcu453ed";
getAllListsForBoardID(boardID, printLists);

function printLists(error, lists) {
  if (error) {
    console.error(error.message);
  } else {
    console.log("Logging from test file.");
    console.log("Lists for boardID = " + boardID);
    console.log(lists);
  }
}
