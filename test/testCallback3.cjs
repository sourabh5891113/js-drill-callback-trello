/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given 
    data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const getAllCardsForListID = require("../callback3.cjs");

let listID = "qwsa221";
getAllCardsForListID(listID, printCards);

function printCards(error, cards) {
  if (error) {
    console.error(error.message);
  } else {
    console.log("Logging from test file.");
    console.log("cards for listID = " + listID);
    console.log(cards);
  }
}