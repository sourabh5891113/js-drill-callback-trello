/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in
     boards.json and then pass control back to the code that called it by using a callback function.
*/
const getBoardFromID = require("../callback1.cjs");

let boardID = "mcu453ed";
getBoardFromID(boardID, printBoardInfo);

function printBoardInfo(error,boardInfo) {
    if(error){
        console.error(error.message);
    }else{
        console.log("Logging from test file.");
        console.log(boardInfo.name +"'s info is ", boardInfo);
    }
}
