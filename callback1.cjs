const fs = require("fs");

function getBoardFromID(boardID, callback) {
  if (typeof boardID === "string" && typeof callback === "function") {
    // setTimeout(()=>{
        fs.readFile("../boards.json", "utf-8", (err, data) => {
          if (err) {
            callback(err);
          } else {
            data = JSON.parse(data);
            let borad = data.find((board) => board.id === boardID);
            if(borad !== undefined){
                setTimeout(callback,1000,null,borad);
            }else{
                callback(new Error("Board not found!!"));
            }
          }
        });
    // },1000);
  }
}

module.exports = getBoardFromID;
