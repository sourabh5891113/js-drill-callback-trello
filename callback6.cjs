const fs = require("fs");
const getBoardFromID = require("./callback1.cjs");
const getAllListsForBoardID = require("./callback2.cjs");
const getAllCardsForListID = require("./callback3.cjs");

function getThanosInfoAndAllListsCards(boardName) {
  if (typeof boardName === "string") {
    setTimeout(() => {
      fs.readFile("../boards.json", "utf-8", (err, data) => {
        if (err) {
          console.error(err);
        } else {
          data = JSON.parse(data);
          let board = data.find((board) => board.name === boardName);
          let boardID = board.id;
          getBoardFromID(boardID, (err, data) => {
            if (err) {
              console.error(err);
            } else {
              console.log(boardName + " board info =>", data);
              getAllListsForBoardID(boardID, (err, data) => {
                if (err) {
                  console.error(err);
                } else {
                  console.log("Lists for the Thanos board => ", data);
                  data.forEach((list) => {
                    getAllCardsForListID(list.id, (err, data) => {
                      if (err) {
                        console.error("For "+ list.name +" list => "+err.message);
                      } else {
                        console.log(
                          "Cards for " + list.name + " list =>",
                          data
                        );
                      }
                    });
                  });
                }
              });
            }
          });
        }
      });
    }, Math.random() * 2000);
  } else {
    console.log("All arguments should be a string");
  }
}

module.exports = getThanosInfoAndAllListsCards;
